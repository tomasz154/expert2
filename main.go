package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/chromedp/chromedp"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const quoteSiteURL = "https://www.apptic.me/projects/jargon/"

type expertType struct {
	date     string
	buzzword string
	quote    string
}

type response struct {
	Title   string `json:"title"`
	Message string `json:"message"`
	Quote   string `json:"quote"`
}

var expert *expertType

func main() {

	http.HandleFunc("/", getResponse)
	http.ListenAndServe(":3000", nil)
}

func getResponse(writer http.ResponseWriter, request *http.Request) {
	date := time.Now().Format("01-02-2006")
	if expert == nil || expert.date != date {
		initializeExpert(date)
	}

	response := response{
		Title:   "Who is Sebastianin today?",
		Message: fmt.Sprintf("Today Sebastianin is %s expert.", expert.buzzword),
		Quote:   expert.quote,
	}

	writer.Header().Add("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(response)
}

func initializeExpert(date string) {
	expert = &expertType{
		date:     date,
		quote:    getQuote(),
		buzzword: getBuzzword(),
	}
}

func getQuote() string {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	var value string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(quoteSiteURL),
		chromedp.WaitVisible("#phraseBox"),
		chromedp.Text("#phraseBox", &value))

		err != nil {
		panic(err)
	}

	return value
}

func getBuzzword() string {
	data, _ := ioutil.ReadFile("buzzword.txt")
	stringsSplit := strings.Split(string(data), "\n")
	rand.Seed(time.Now().Unix())
	return stringsSplit[rand.Intn(len(stringsSplit))]
}
