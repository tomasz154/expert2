module expert-generator

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/chromedp/cdproto v0.0.0-20200116234248-4da64dd111ac
	github.com/chromedp/chromedp v0.5.3
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
)
